.. KrystalAI documentation master file, created by
   sphinx-quickstart on Thu Dec 27 19:12:38 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to KrystalAI's documentation!
=====================================
IE NLP Engine is a module which is implemented in python3.6, with major responsibility being automatic information extraction from legal documents. This module consist of 9 sub modules. They are Workflow Allocation, OCR, Doc Normalization, Paragraph Generator, NLP Feature, Tokenization, Temporal Tagger, Field Extraction and ML Training respectively. For every submodule, input is docID except ML module. Based on this docID, relevant information (inputs, parameters, etc) will be extracted from DB. There will be one inputObject class, outputObject class and metadataObject class for each submodule. metadataObject will call the DB using intermidiate Wrapper file. All the procedures will be in Wrapper class. It will fetch the required files from fileserver. inputObject will call the main method of each submodule to pass the inputs accessed from metadataObject. outputObject will upload the output to fileserver or DB depending on the submodule. The output json of NLP Feature, Tokenization and Temporal Tagger is the mapped json where the keys are mapped to alphabates(which takes less memory). The mapping of the json is defined in external file.

Workflow Allocation
=====================================

.. toctree::
   :maxdepth: -1
   :caption: Contents:

.. automodule:: workflow_allocation_engine.WorkflowAllocationEngine
    :members: main

Testing Phase:

.. automodule:: ieNLPEngine_testing
    :members: workflow_allocation


OCR
=====================================

.. toctree::
   :maxdepth: -1
   :caption: Contents:

.. automodule:: ocr_engine.OcrEngine
    :members: main

Testing Phase:

.. automodule:: ieNLPEngine_testing
    :members: ocr


Document Normalization
=====================================

.. toctree::
   :maxdepth: -1
   :caption: Contents:

.. automodule:: doc_normalization_engine.DocNormalizationEngine
    :members: main

Testing Phase:

.. automodule:: ieNLPEngine_testing
    :members: doc_normalization


Paragraph Generator
=====================================

.. toctree::
   :maxdepth: -1
   :caption: Contents:

.. automodule:: paraGenerator_engine.ParaGeneratorEngine
    :members: main

Testing Phase:

.. automodule:: ieNLPEngine_testing
    :members: para_generator


Tokenization
=====================================

.. toctree::
   :maxdepth: -1
   :caption: Contents:

.. automodule:: tokenizer_engine.TokenizerEngine
    :members: main

Testing Phase:

.. automodule:: ieNLPEngine_testing
    :members: Tokenizer


NLP Feature
=====================================

.. toctree::
   :maxdepth: -1
   :caption: Contents:

.. automodule:: nlp_feature_engine.NLPFeatureEngine
    :members: main

Testing Phase:

.. automodule:: ieNLPEngine_testing
    :members: nlp_feature


Temporal Tagger
=====================================

.. toctree::
   :maxdepth: -1
   :caption: Contents:

.. automodule:: temporal_tagger_engine.TemporalTaggerEngine
    :members: main

Testing Phase:

.. automodule:: ieNLPEngine_testing
    :members: temporal_tagger


Field Extraction
=====================================

.. toctree::
   :maxdepth: -1
   :caption: Contents:

.. automodule:: fieldExtraction.FieldExtraction
    :members: main

Testing Phase:

.. automodule:: ieNLPEngine_testing
    :members: field_extraction


ML Training
=====================================

.. toctree::
   :maxdepth: -1
   :caption: Contents:

.. automodule:: inputObjectML.InputObjectML
    :members: trigger
